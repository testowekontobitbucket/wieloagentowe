package com.company.Zad2;

import jade.core.Agent;

import java.util.LinkedList;

public class ConsumerAgent extends Agent {
    public LinkedList<String> tokens;

    @Override
    public void setup() {
        tokens = new LinkedList<>();
        addBehaviour(new GetTokenBehaviour(this, 2000));
    }

    @Override
    public void takeDown() {

    }
}
