package com.company.Zad2;

import jade.core.behaviours.Behaviour;

import java.util.UUID;

public class GenerateTokenBehaviour extends Behaviour {
    private final ProducerAgent agent;
    private final int frequency;
    private final int tokensCount;

    public GenerateTokenBehaviour(ProducerAgent agent, int frequency, int tokensCount) {
        this.agent = agent;
        this.frequency = frequency;
        this.tokensCount = tokensCount;
    }

    @Override
    public void action() {
        int generatedTokensCount = 0;
        long timer = 0;
        while(generatedTokensCount < tokensCount) {
            if(timer % frequency == 0) {
                String token = UUID.randomUUID().toString();
                agent.tokensQueue.add(token);
                generatedTokensCount++;
            }
            timer++;
        }

        agent.removeBehaviour(this);
    }

    @Override
    public boolean done() {
        return false;
    }
}
