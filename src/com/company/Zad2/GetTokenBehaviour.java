package com.company.Zad2;

import jade.core.AID;
import jade.core.LifeCycle;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class GetTokenBehaviour extends Behaviour {
    private final ConsumerAgent agent;
    private final int frequency;

    public GetTokenBehaviour(ConsumerAgent agent, int frequency) {
        this.agent = agent;
        this.frequency = frequency;
    }

    @Override
    public void action() {
        long timer = 0;
        boolean hasTokens = true;

        while(hasTokens) {
            if(timer % frequency == 0) {
                ACLMessage aclMessage = new ACLMessage(ACLMessage.INFORM);
                aclMessage.addReceiver(new AID("ProducerAgent1", AID.ISLOCALNAME));
                aclMessage.setContent("getToken");
                agent.send(aclMessage);

                aclMessage = agent.blockingReceive();

                if(aclMessage.getContent().equals("EMPTY")) {
                    hasTokens = false;
                    continue;
                }

                agent.tokens.add(aclMessage.getContent());
            }
            timer++;
        }

        System.out.println(agent.getLocalName() + ": " + agent.tokens.size());
        agent.removeBehaviour(this);
    }

    @Override
    public boolean done() {
        return false;
    }
}
