package com.company.Zad2;

import jade.core.Agent;

import java.util.PriorityQueue;
import java.util.Queue;

public class ProducerAgent extends Agent {

    public Queue<String> tokensQueue = new PriorityQueue<>();

    @Override
    public void setup() {
        addBehaviour(new GenerateTokenBehaviour(this, 2000, 2000));
        addBehaviour(new SendTokenBehaviour(this));
    }

    @Override
    public void takeDown() {
    }
}
