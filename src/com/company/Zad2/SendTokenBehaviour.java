package com.company.Zad2;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

import java.util.NoSuchElementException;

public class SendTokenBehaviour extends Behaviour {
    private ProducerAgent agent;

    public SendTokenBehaviour(ProducerAgent producerAgent) {
        agent = producerAgent;
    }

    @Override
    public void action() {
        ACLMessage aclMessage = agent.blockingReceive();
        String content = aclMessage.getContent();
        if(content.equals("getToken")) {
            try {
                String token = agent.tokensQueue.remove();

                ACLMessage aclResponse = new ACLMessage(ACLMessage.INFORM);
                aclResponse.addReceiver(aclMessage.getSender());
                aclResponse.setContent(token);
                agent.send(aclResponse);
            }
            catch (NoSuchElementException e) {
                ACLMessage aclResponse = new ACLMessage(ACLMessage.FAILURE);
                aclResponse.addReceiver(aclMessage.getSender());
                aclResponse.setContent("EMPTY");
                agent.send(aclResponse);
            }

        }
    }

    @Override
    public boolean done() {
        return false;
    }
}
