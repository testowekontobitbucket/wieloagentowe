package com.company;

import com.company.Zad2.Zad2JadeBootThread;
import com.company.Zad3.Zad3JadeBootThread;

public class Main {

    public static void main(String[] args) throws NoSuchMethodException, ClassNotFoundException {
        //new Zad2JadeBootThread().run();
        new Zad3JadeBootThread().run();
    }
}
