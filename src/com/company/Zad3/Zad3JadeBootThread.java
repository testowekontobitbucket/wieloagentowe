package com.company.Zad3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Zad3JadeBootThread extends Thread {

    private final String jadeBoot_CLASS_NAME = "jade.Boot";

    private final String MAIN_METHOD_NAME = "main";

    //add the <agent-local-name>:<fully-qualified-agent-class> name here;
// you can add more than one by semicolon separated values.
    private final String ACTOR_NAMES_args = "" +
            "DistributorAgent1:com.company.Zad3.DistributorAgent;" +
            "CountingAgent1:com.company.Zad3.CountingAgent;" +
            "CountingAgent2:com.company.Zad3.CountingAgent;" +
            "CountingAgent3:com.company.Zad3.CountingAgent;" +
            "CountingAgent4:com.company.Zad3.CountingAgent;";

    private final String GUI_args = "-gui";

    private final Class<?> secondClass;

    private final Method main;

    private final String[] params;

    public Zad3JadeBootThread() throws ClassNotFoundException, SecurityException, NoSuchMethodException {
        secondClass = Class.forName(jadeBoot_CLASS_NAME);
        main = secondClass.getMethod(MAIN_METHOD_NAME, String[].class);
        params = new String[]{GUI_args, ACTOR_NAMES_args};
    }

    @Override
    public void run() {
        try {
            main.invoke(null, new Object[]{params});
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            ex.printStackTrace();
        }

    }
}