package com.company.Zad3;

import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class FetchAndMultiplyBehaviour extends Behaviour {
    CountingAgent agent;

    public FetchAndMultiplyBehaviour(CountingAgent agent) {
        this.agent = agent;
    }

    @Override
    public void action() {
        ACLMessage aclMessage = agent.blockingReceive();
        if(aclMessage.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {
            ACLMessage aclResponse = new ACLMessage(ACLMessage.INFORM);
            aclResponse.addReceiver(aclMessage.getSender());

            double result = 0;
            if(agent.hasFailure()) {
                System.out.println(agent.getLocalName() + " AWARIA");
                aclResponse.setPerformative(ACLMessage.FAILURE);
            }
            else {
                double toMultiply = Double.parseDouble(aclMessage.getContent());
                result = toMultiply * agent.multiplier;
                aclResponse.setContent(String.valueOf(result));

                try {
                    Thread.sleep(agent.getPause());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            agent.send(aclResponse);
            agent.addBehaviour(new ReadyToCountBehaviour(agent));
            System.out.println(agent.getLocalName() + ": multiplying result " + result);
        }
    }

    @Override
    public boolean done() {
        return false;
    }
}
