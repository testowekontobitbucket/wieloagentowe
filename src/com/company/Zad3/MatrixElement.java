package com.company.Zad3;

public class MatrixElement {
    public int x;
    public int y;
    public String agent;

    public MatrixElement(int x, int y, String agent) {
        this.x = x;
        this.y = y;
        this.agent = agent;
    }
}
