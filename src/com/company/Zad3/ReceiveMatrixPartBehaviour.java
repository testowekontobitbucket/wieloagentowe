package com.company.Zad3;

import jade.core.NotFoundException;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class ReceiveMatrixPartBehaviour extends Behaviour {
    DistributorAgent agent;

    public ReceiveMatrixPartBehaviour(DistributorAgent distributorAgent) {
        agent = distributorAgent;
    }

    @Override
    public void action() {
        ACLMessage aclMessage = agent.blockingReceive();
        if(aclMessage.getPerformative() == ACLMessage.INFORM) {
            int x = 0, y = 0;
            try {
                MatrixElement matrixElement = agent.getMatrixElementForAgent(aclMessage.getSender().getLocalName());
                x = matrixElement.x;
                y = matrixElement.y;
                agent.distributedList.remove(matrixElement);
            } catch (NotFoundException e) {
                e.printStackTrace();
            }

            agent.resultMatrix[x][y] = Double.valueOf(aclMessage.getContent());
            agent.statusMatrix[x][y] = DistributorAgent.StatusEnum.CALCULATED.ordinal();
        }

        System.out.println("Wynik:");
        for (int i = 0; i < agent.resultMatrix.length; i++) {
            for(int j=0; j < agent.resultMatrix[0].length; j++) {
                System.out.print(agent.resultMatrix[i][j] + " ");
            }
            System.out.print("\n");
        }
    }

    @Override
    public boolean done() {
        return false;
    }
}
