package com.company.Zad3;

import jade.core.Agent;
import jade.core.NotFoundException;

import java.util.LinkedList;
import java.util.Random;
import java.util.Vector;

public class DistributorAgent extends Agent {
    public Double[][] matrix;
    public Double[][] resultMatrix;
    public int[][] statusMatrix;
    public LinkedList<MatrixElement> distributedList;

    public enum StatusEnum {
        EMPTY, WAITING, CALCULATED
    }

    public final int width = 2;
    public final int height = 2;

    public void setup() {
        generateMatrix();

        addBehaviour(new SendMatrixPartsBehaviour(this));
        addBehaviour(new ReceiveMatrixPartBehaviour(this));
    }

    private void generateMatrix() {
        matrix = new Double[width][height];
        resultMatrix = new Double[width][height];
        statusMatrix = new int[width][height];
        distributedList = new LinkedList<>();

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                matrix[i][j] = Math.random();
                statusMatrix[i][j] = StatusEnum.EMPTY.ordinal();
            }
        }
    }

    public MatrixElement getMatrixElementForAgent(String agentName) throws NotFoundException {
        for (MatrixElement matrixElement : this.distributedList) {
            if(matrixElement.agent.equals(agentName)) {
                return matrixElement;
            }
        }
        throw new NotFoundException();
    }
}
