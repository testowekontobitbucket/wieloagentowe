package com.company.Zad3;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class SendMatrixPartsBehaviour extends Behaviour {
    DistributorAgent agent;
    public SendMatrixPartsBehaviour(DistributorAgent distributorAgent) {
        agent = distributorAgent;
    }

    @Override
    public void action() {
        ACLMessage aclMessage = agent.blockingReceive();
        if(aclMessage.getPerformative() == ACLMessage.PROPAGATE) {
            ACLMessage aclResponse = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
            aclResponse.addReceiver(aclMessage.getSender());
            for(int i = 0; i < agent.width; i++) {
                for(int j = 0; j < agent.height; j++) {
                    if(agent.statusMatrix[i][j] == DistributorAgent.StatusEnum.EMPTY.ordinal()
                        && aclMessage.getPerformative() != ACLMessage.FAILURE) {
                        agent.statusMatrix[i][j] = DistributorAgent.StatusEnum.WAITING.ordinal();
                        aclResponse.setContent(agent.matrix[i][j].toString());
                        agent.distributedList.add(new MatrixElement(i, j, aclMessage.getSender().getLocalName()));
                        agent.send(aclResponse);
                        System.out.println(agent.getLocalName() + " has sent matrix to " + aclMessage.getSender().getLocalName());
                        break;
                    }
                }
            }
        }
    }

    @Override
    public boolean done() {
        return false;
    }
}
