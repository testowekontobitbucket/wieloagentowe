package com.company.Zad3;

import jade.core.Agent;

import java.util.Date;
import java.util.Random;

public class CountingAgent extends Agent {
    public final int multiplier = 2;
    private int pause;

    public void setup() {
        this.pause = (new Random().nextInt(1000)) + 500;

        addBehaviour(new ReadyToCountBehaviour(this));
        addBehaviour(new FetchAndMultiplyBehaviour(this));
    }

    public int getPause() {
        return pause;
    }

    public boolean hasFailure() {
        int value = new Random().nextInt(5);
        if(value % 5 == 0) return true;
        else return false;
    }
}
