package com.company.Zad3;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;

public class ReadyToCountBehaviour extends Behaviour {
    CountingAgent agent;

    public ReadyToCountBehaviour(CountingAgent agent) {
        this.agent = agent;
    }

    @Override
    public void action() {
        ACLMessage aclMessage = new ACLMessage(ACLMessage.PROPAGATE);
        aclMessage.addReceiver(new AID("DistributorAgent1", AID.ISLOCALNAME));
        agent.send(aclMessage);
        agent.removeBehaviour(this);
        System.out.println(agent.getLocalName() + " says he is ready");
    }

    @Override
    public boolean done() {
        return false;
    }
}
